@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            选择批次: 
            <select id="select_batch" class="form-control" name="batch">
                @foreach ($batchs as $pici)
                    <option value="{{$pici['batch']}}">{{$pici['batch']}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table class="table table-striped">
                <caption>批次: {{$batch}} </caption>
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>年龄</th>
                        <th>性别</th>
                        <th>电话</th>
                        <th>身份证</th>
                        <th>学历</th>
                        <th>学校</th>
                        <th>专业</th>
                        <th>成绩操作</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($applies as $apply)
                    <tr>
                        <td>{{$apply->user->name}}</td>
                        <td>{{$apply->user->age()}}</td>
                        <td>{{$apply->user->sex}}</td>
                        <td>{{$apply->user->mobile}}</td>
                        <td>{{$apply->user->idnumber}}</td>
                        <td>{{$apply->degree}}</td>
                        <td>{{$apply->university}}</td>
                        <td>{{$apply->study}}</td>
                        <td>
                            <button id="{{$apply->id}}_0" class="set_result btn btn-sm @if($apply->result=='') btn-primary @else btn-default @endif">未出成绩</button>
                            <button id="{{$apply->id}}_1" class="set_result btn btn-sm @if($apply->result=='1') btn-primary @else btn-default @endif">未通过</button>
                            <button id="{{$apply->id}}_2" class="set_result btn btn-sm @if($apply->result=='2') btn-primary @else btn-default @endif">通过</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="//cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
<script>
$('#select_batch').change(function(evnt){ location.href = '/admin/applies/' + $(this).val(); });

$('.set_result').click(function(evnt) {
    //console.log($(this).attr('id'));
    var id = $(this).attr('id');
    var apply_id = id.split("_")[0];
    var result = id.split("_")[1];
    //console.log(apply_id);console.log(result);
    $('#'+apply_id+"_0").removeClass('btn-primary').addClass('btn-default');
    $('#'+apply_id+"_1").removeClass('btn-primary').addClass('btn-default');
    $('#'+apply_id+"_2").removeClass('btn-primary').addClass('btn-default');
    
    $.ajax({
        url: "/admin/applies/"+apply_id+"/set_result/"+result,
    }).success(function() {
        $('#'+apply_id+"_"+result).removeClass('btn-default').addClass('btn-primary');
    });
});
</script>
@endsection
