@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">成绩查询</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/result') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">姓名</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('idnumber') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">身份证号</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="idnumber" value="{{ old('idnumber') }}">

                                @if ($errors->has('idnumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idnumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('batch') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">批次</label>

                            <div class="col-md-6">
                                <select id="" class="form-control" name="batch">
                                    @foreach ($batchs as $batch)
                                        <option value="{{$batch['batch']}}">{{$batch['batch']}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('batch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('batch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>查询
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">成绩</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-md-4 control-label">姓名</label>
                            <div class="col-md-6">
                                {{ $name or "" }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">身份证</label>
                            <div class="col-md-6">
                                {{ $idnumber or "" }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">批次</label>
                            <div class="col-md-6">
                                {{ $pici or "" }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">成绩</label>

                            <div class="col-md-6">
                                @if ($apply && $apply->result == "0")
                                    未出成绩
                                @elseif ($apply && $apply->result == "1")
                                    未通过
                                @elseif ($apply && $apply->result == "2")
                                    通过
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
