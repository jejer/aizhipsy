@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">申请</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/apply') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">姓名</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('idnumber') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">身份证号</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="idnumber" value="{{ old('idnumber') }}">

                                @if ($errors->has('idnumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idnumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('idnumber_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">确认身份证号</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="idnumber_confirmation">

                                @if ($errors->has('idnumber_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idnumber_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">手机号</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}">

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">性别</label>

                            <div class="col-md-6">
                                <select id="" class="form-control" name="sex">
                                    <option value="男">男</option>
                                    <option value="女">女</option>
                                </select>

                                @if ($errors->has('sex'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('degree') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">学历</label>

                            <div class="col-md-6">
                                <select id="" class="form-control" name="degree">
                                    <option value="专科">专科</option>
                                    <option value="本科">本科</option>
                                    <option value="研究生及以上">研究生及以上</option>
                                </select>

                                @if ($errors->has('degree'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('degree') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('university') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">学校</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="university" value="{{ old('university') }}">

                                @if ($errors->has('university'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('university') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('study') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">专业</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="study" value="{{ old('study') }}">

                                @if ($errors->has('study'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('study') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('batch') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">批次</label>

                            <div class="col-md-6">
                                <select id="" class="form-control" name="batch">
                                    @foreach ($batchs as $batch)
                                        <option value="{{$batch}}">{{$batch}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('batch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('batch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>提交
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
