<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        App\User::create(['name' => '阚永锋', 'email' => 'uy9001@gmail.com', 
             'mobile' => '13375698190', 'idnumber' => '', 'password' => bcrypt('uy9001')]);
        App\User::create(['name' => 'jejer', 'email' => 'zjejer@gmail.com', 
             'mobile' => '18605819527', 'idnumber' => '18605819527', 'password' => bcrypt('jejer')]);
    }
}
