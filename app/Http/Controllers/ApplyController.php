<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ApplyController extends Controller
{
    public function create() {
        $batchs = array();
        $carbon = \Carbon\Carbon::now();
        $batch_start = $carbon->copy();
        if ($carbon->month <= 5) {
            $batch_start->month = 5;
        } elseif ($carbon->month > 5 && $carbon->month <= 11) {
            $batch_start->month = 11;
        } else {
            $batch_start->year += 1;
            $batch_start->month = 5;
        }
        for ($i = 0; $i < 6; $i++) {
            array_push($batchs, sprintf("%d年%d月", $batch_start->year, $batch_start->month));
            $batch_start->month += 6;
        }
        return view('apply', compact('batchs'));
    }
    
    public function store(Request $request) {
        //dd($request);
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:10',
            'email' => 'required|email|max:255|unique:users',
            'idnumber' => 'required|between:18,18|confirmed|unique:users',
            'mobile' => 'required|between:11,11',
            'sex' => 'required',
            'degree' => 'required',
            'university' => 'required',
            'study' => 'required',
            'batch' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('apply')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $user = \App\User::create(['name' => $request->input('name'), 
                                'email' => $request->input('email'), 
                                'mobile' => $request->input('mobile'), 
                                'idnumber' => $request->input('idnumber'), 
                                'sex' => $request->input('sex'), 
                                'password' => bcrypt($request->input('name'))]);
        
        $apply = new \App\Apply(['degree' => $request->input('degree'),
                                'university' => $request->input('university'),
                                'study' => $request->input('study'),
                                'batch' => $request->input('batch'),
                                'result' => '0']);
        $user->applies()->save($apply);
        
        return "申请成功";
    }
    
    public function result() {
        $apply = null;
        $batchs = \App\Apply::select('batch')->groupBy('batch')->get()->toArray();

        return view('result', compact('batchs', 'apply'));
    }
    
    public function result_show(Request $request) {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:10',
            'idnumber' => 'required|between:18,18',
        ]);

        if ($validator->fails()) {
            return redirect('result')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $user = \App\User::where('idnumber', $request->input('idnumber'))
                            ->where('name', $request->input('name'))->firstOrFail();
        
        $apply = $user->applies()->where('batch', $request->input('batch'))->firstOrFail();
        $batchs = \App\Apply::select('batch')->groupBy('batch')->get()->toArray();
        $name = $user->name;
        $idnumber = $user->idnumber;
        $pici = $apply->batch;
        return view('result', compact('user', 'apply', 'batchs', 'name', 'idnumber', 'pici'));
    }
}
