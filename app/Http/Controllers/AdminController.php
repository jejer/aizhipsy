<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Http\Requests;

class AdminController extends Controller
{
    public function __construct() {
        $user = Auth::user();
        if (!$user || $user->email != 'uy9001@gmail.com') {
            Auth::logout();
            return redirect('/admin')->send();
        }
    }
    
    public function applys($batch = null) {
        $batchs = array(array('batch' => '请选择'));
        $batchs = array_merge_recursive($batchs, \App\Apply::select('batch')->groupBy('batch')->get()->toArray());

        $applies = array();
        if (!empty($batch)) {
            $applies = \App\Apply::where('batch', $batch)->get();
        } 
        
        return view('update_apply_result', compact('applies', 'batch', 'batchs'));
    }
    
    public function set_result($apply_id, $result) {
        $apply = \App\Apply::where('id', $apply_id)->firstOrFail();
        $apply->result = $result;
        $apply->save();
        
        return "ok";
    }
}
