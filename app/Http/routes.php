<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//报名
Route::get('/apply', 'ApplyController@create');
Route::post('/apply', 'ApplyController@store');

//成绩查询
Route::get('/result', 'ApplyController@result');
Route::post('/result', 'ApplyController@result_show');

//成绩录入
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
    Route::get('/', function() { return redirect('/admin/applies'); });
    Route::get('applies/{batch?}', 'AdminController@applys');
    Route::get('applies/{apply_id}/set_result/{result}', 'AdminController@set_result');
});


Route::auth();
