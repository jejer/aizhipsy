<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apply extends Model
{
    protected $fillable = [
        'degree', 'university', 'study', 'batch',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
