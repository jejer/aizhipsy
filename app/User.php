<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'idnumber', 'mobile', 'sex',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function applies() {
        return $this->hasMany('App\Apply');
    }
    
    public function age() {
        $now = \Carbon\Carbon::now();
        $birth_year = substr($this->idnumber, 6, 4);
        return $now->year - $birth_year;
    }
}
